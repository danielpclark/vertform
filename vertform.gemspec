$:.push File.expand_path("../lib", __FILE__)
require "vertform/version"
 
Gem::Specification.new do |s|
  s.name        = 'vertform'
  s.version     = VertForm::VERSION
  s.licenses    = ['Attribution-NonCommercial-NoDerivatives 4.0 International']
  s.summary     = "Rails vertical form generator from a Hash."
  s.description = 'Rails vertical form generator from a Hash. {"label" => "rails code"} '
  s.authors     = ["Daniel P. Clark / 6ftDan(TM)"]
  s.email       = 'webmaster@6ftdan.com'
  s.files       = ['lib/vertform/version.rb', 'lib/vertform.rb', 'LICENSE', 'README.md']
  s.homepage    = 'https://bitbucket.org/danielpclark/vertform'
  s.platform    = 'ruby'
  s.require_paths = ['lib']
  s.required_ruby_version = '>= 1.9.3'
end
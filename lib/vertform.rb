

class VertForm
	attr_accessor :table_params, :label_params, :form_fields

	def initialize(form_fields = {})
		@table_params = {
			"class" => "table"
		}
		@td_label_params = {
			"style" => "width: 32%;"
		}
		@label_params = {
			"class" => "pull-right",
			"style" => "margin-top: 4px;"
		}
		@td_input_params = {
			"style" => "width: 68%;"
		}
		@form_fields = form_fields
	end

	def rend_form(form_fields = @form_fields)
		out_string = ""
		out_string .
		concat "<table " .
		concat pair_json(@table_params) .
		concat ">\n"
		form_fields.each { |label,field|
			out_string .
			concat "  <tr>\n    <td " .
			concat pair_json(@td_label_params) .
			concat ">\n      " .
			concat "<span " .
			concat pair_json(@label_params) .
			concat ">\n        " .
			concat(label) .
			concat "\n      </span>\n    </td>\n    <td " .
			concat pair_json(@td_input_params) .
			concat ">\n      <%= " .
			concat(field) .
			concat " %>\n    </td>\n  </tr>\n"
		}
		out_string . concat "</table>"
	end

	private
	def pair_json(x)
		return "" unless x.is_a? Hash
		x.map {|a,b| "#{a}='#{b}'"}.join(" ")
	end
end
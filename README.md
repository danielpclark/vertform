# Vertical Form (vertform)

Author:: Daniel P. Clark  [6ftdan@gmail.com](mailto:6ftdan@gmail.com)

License:: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](http://creativecommons.org/licenses/by-nc-nd/4.0/)

Give it a hash of `{"lable" => "rails code"}` and it builds a vertical table with it.  Now you can save your forms in your favorite format such as JSON for easy updating.

    j = {"#" => "f.text_field :tag, placeholder: 'tag'"}
    v = VertForm.new j
    print v.rend_form
    <table class='table'>
      <tr>
        <td>
          <span class='pull-right' style='margin-top: 4px;'>
            #
          </span>
        </td>
        <td>
          <%= f.text_field :tag, placeholder: 'tag' %>
        </td>
      </tr>
    </table> => nil